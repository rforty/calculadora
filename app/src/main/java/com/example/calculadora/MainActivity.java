package com.example.calculadora;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    //CREAMOS NUESTRAS VARIABLES
    private EditText et1;
    private EditText et2;
    private TextView res;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //GUARDAMOS EN NUESTRAS VARIABLES LOS VALORES DE NUESTROS COMPONENTES
        //HACIENDO CASTING LE DAMOS EL TIPO DE OBJETO QUE QUERAMOS QUE SEAN
        et1 = (EditText)findViewById(R.id.text_num1);
        et2 = (EditText)findViewById(R.id.text_num2);
        res = (TextView)findViewById(R.id.txt_resultado);

    }
    //METODO PARA REALIZAR LA OPERACION
    public void multi (View view){
        //CAPTURAMOS EL TEXTO DE LAS COMPONENTES
        String valor1 = et1.getText().toString();
        String valor2 = et2.getText().toString();
        //PARSEAMOS DE STRING A ENTERO
        int num1= Integer.parseInt(valor1);
        int num2= Integer.parseInt(valor2);
        //REALIZAMOS LA OPERACION
        int multi=(num1*num2);
        //PARSEAMOS DE ENTERO A STRING
        String result=String.valueOf(multi);
        //ENVIAMOS EL RESULTADO AL COMPONENTE TextView
        res.setText(result);


        //ff

    }
}
